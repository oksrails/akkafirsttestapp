﻿namespace AkkaFirstTestApp.Domains
{
    public class AkkaModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual Group Group { get; set; }
        public double StatA { get; set; }
        public double StatB { get; set; }
        public double StatC { get; set; }
        public virtual List<AkkaSubModel> AkkaSubModels { get; set; }

        public void CalculateStats()
        {
            StatB = CalculateStatB();
            StatC = CalculateStatC();
        }

        private double CalculateStatB()
        {
            if (AkkaSubModels?.Count > 0)
            {
                var subStat = AkkaSubModels.Select(s => s.SubStatC * s.SubStatB / s.SubStatA).Sum();
                return StatA / subStat;
            }

            return StatA / 429;
        }

        private double CalculateStatC()
        {
            return StatB / StatA;
        }
    }
}
