﻿namespace AkkaFirstTestApp.Domains
{
    public class AkkaSubModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public double SubStatA { get; set; }
        public double SubStatB { get; set; }
        public double SubStatC { get; set; }

        public void CalculateSubStats()
        {
            SubStatB = CalculateSubStatB();
            SubStatC = CalculateSubStatC();
        }

        private double CalculateSubStatB()
        {
            return SubStatA + 313 * 0.3 + 11;
        }

        private double CalculateSubStatC()
        {
            return SubStatB + SubStatA + 404 / 340;
        }
    }
}
