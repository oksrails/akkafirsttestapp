﻿namespace AkkaFirstTestApp.Commands
{
    public sealed class GroupCalculationCommad
    {
        public Guid CommandId { get; init; }
        public Guid GroupId { get; init; }

        public GroupCalculationCommad(
            Guid commandId,
            Guid groupId)
        {
            CommandId = commandId;
            GroupId = groupId;
        }
    }
}
