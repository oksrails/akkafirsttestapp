﻿namespace AkkaFirstTestApp.Commands
{
    public class GroupCalculationFinishedCommand
    {
        public Guid CommandId { get; init; }
        public Guid GroupId { get; init; }

        public GroupCalculationFinishedCommand(
            Guid commandId,
            Guid groupId)
        {
            CommandId = commandId;
            GroupId = groupId;
        }
    }
}
