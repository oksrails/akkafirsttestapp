﻿namespace AkkaFirstTestApp.Commands
{
    public class InitiateGroupCalculationCommand
    {
        public Guid CommandId { get; init; }
        public Guid GroupId { get; init; } 

        public InitiateGroupCalculationCommand(
            Guid commandId,
            Guid groupId)
        {
            CommandId = commandId;
            GroupId = groupId;
        }
    }
}
