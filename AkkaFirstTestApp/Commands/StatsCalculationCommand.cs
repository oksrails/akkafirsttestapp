﻿namespace AkkaFirstTestApp.Commands
{
    public sealed class StatsCalculationCommand
    {
        public Guid CommandId { get; init; }
        public Guid ModelId { get; init; }

        public StatsCalculationCommand(
            Guid commandId,
            Guid modelId)
        {
            CommandId = commandId;
            ModelId = modelId;
        }
    }
}
