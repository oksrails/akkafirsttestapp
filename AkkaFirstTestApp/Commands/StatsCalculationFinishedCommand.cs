﻿namespace AkkaFirstTestApp.Commands
{
    public class StatsCalculationFinishedCommand
    {
        public Guid CommandId { get; init; }
        public Guid ActorId { get; init; }
        public Guid GroupId { get; init; }

        public StatsCalculationFinishedCommand(
            Guid commandId,
            Guid actorId,
            Guid groupId)
        {
            CommandId = commandId;
            ActorId = actorId;
            GroupId = groupId;
        }
    }
}
