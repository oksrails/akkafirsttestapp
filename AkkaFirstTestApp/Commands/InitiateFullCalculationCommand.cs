﻿namespace AkkaFirstTestApp.Commands
{
    public sealed class InitiateFullCalculationCommand
    {
        public Guid CommandId { get; set; }

        public InitiateFullCalculationCommand(Guid commandId)
        {
            CommandId = commandId;
        }
    }
}
