﻿using Akka.Actor;
using Akka.Event;
using Akka.Logger.Serilog;
using AkkaFirstTestApp.Commands;
using AkkaFirstTestApp.Domains;
using AkkaFirstTestApp.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace AkkaFirstTestApp.Actors
{
    public class GroupManager : UntypedActor
    {
        private readonly AkkaDbContext _dbContext;
        private Group _group;
        private List<AkkaModel> _akkaModels = new List<AkkaModel>();
        private readonly ILoggingAdapter _logger;
        private Guid ActorId { get; init; }

        private Dictionary<Guid, IActorRef> modelIdToActor = new Dictionary<Guid, IActorRef>();
        private Dictionary<IActorRef, Guid> actorToModelId = new Dictionary<IActorRef, Guid>();

        public GroupManager(
            Guid actorId,
            Group group,
            AkkaDbContext dbContext)
        {
            ActorId = actorId;
            _group = group;
            _dbContext = dbContext;
            _logger = Context.GetLogger<SerilogLoggingAdapter>() 
                ?? throw new ArgumentNullException(nameof(SerilogLoggingAdapter));
        }

        protected override void PreStart()
            => _logger.Info($"{nameof(StatsCalculator)}-{ActorId} started");

        protected override void PostStop()
            => _logger.Info($"{nameof(StatsCalculator)}-{ActorId} stopped");

        protected override SupervisorStrategy SupervisorStrategy()
        {
            var strategy = new OneForOneStrategy(
                maxNrOfRetries: 10,
                withinTimeRange: TimeSpan.FromMilliseconds(200),
                localOnlyDecider: ex =>
                {
                    switch (ex)
                    {
                        case TimeoutException te:
                            return Directive.Stop;
                        case ArgumentException are:
                            _logger.Error($"An argument exception was occured: {are.Message}");
                            return Directive.Restart;
                        default:
                            return Directive.Escalate;
                    }
                });

            return strategy;
        }

        protected override void OnReceive(object message)
        {
            switch (message)
            {
                case GroupCalculationCommad com when com.GroupId.Equals(_group.Id):
                    OrchestrateActors();
                    break;
                case GroupCalculationCommad com:
                    _logger.Warning($"Ignoring calculation request for {com.GroupId}. This actor is responsible for {_group.Id}.");
                    break;
                case StatsCalculationFinishedCommand com when com.GroupId.Equals(_group.Id):
                    if (modelIdToActor.TryGetValue(com.ActorId, out var actorRef))
                    {
                        Context.Stop(actorRef);
                        actorToModelId.Remove(actorRef);
                        modelIdToActor.Remove(com.ActorId);
                        if (modelIdToActor.Count == 0)
                        {
                            FinishGroupCalculation();
                            Context.Parent.Tell(new GroupCalculationFinishedCommand(Guid.NewGuid(), _group.Id));
                        }
                    }
                    break;
                case StatsCalculationFinishedCommand com:
                    _logger.Warning($"Ignoring finishing request for {com.GroupId}. This actor is responsible for {_group.Id}.");
                    break;
                case Terminated t:
                    if(actorToModelId.TryGetValue(t.ActorRef, out var actorId))
                    {
                        _logger.Info($"Actor for model has been terminated");
                    }
                    break;
                default:
                    _logger.Warning("Message of unrecognized type has been received");
                    break;
            }
        }

        public static Props Props(Guid actorId, Group group, AkkaDbContext dbContext)
            => Akka.Actor.Props.Create(()
                => new GroupManager(actorId, group, dbContext));

        private void OrchestrateActors()
        {
            _logger.Info($"Creating actors for calculating models in group {ActorId}...");
            _akkaModels = _dbContext
                .AkkaModels
                .Where(m => m.Group.Id.Equals(_group.Id))
                .Include(m => m.Group)
                .Include(m => m.AkkaSubModels)
                .ToList();

            foreach (var model in _akkaModels)
            {
                var modelActor = Context.ActorOf(StatsCalculator.Props(model.Id, model));
                Context.Watch(modelActor);
                actorToModelId.Add(modelActor, model.Id);
                modelIdToActor.Add(model.Id, modelActor);
                modelActor.Tell(new StatsCalculationCommand(Guid.NewGuid(), model.Id));
            }

            Parallel.ForEach(_akkaModels, model =>
            {
                var actorRef = modelIdToActor[model.Id];
                actorRef.Tell(new StatsCalculationCommand(Guid.NewGuid(), model.Id));
            });
        }

        private void FinishGroupCalculation()
        {
            _logger.Info($"All modeles in group {_group.Id} has been calculated. Saving changes...");
            _dbContext.SaveChanges();
            _dbContext.Dispose();
            _logger.Info($"Changes for group {_group.Id} has been saved");
        }
    }
}
