﻿using Akka.Actor;
using Akka.Event;
using Akka.Logger.Serilog;
using AkkaFirstTestApp.Commands;
using AkkaFirstTestApp.Domains;

namespace AkkaFirstTestApp.Actors
{
    public class StatsCalculator : UntypedActor
    {
        private AkkaModel _akkaModel;
        private readonly ILoggingAdapter _logger;
        private Guid ActorId { get; init; }

        public StatsCalculator(
            Guid actorId,
            AkkaModel akkaModel)
        {
            _akkaModel = akkaModel;
            ActorId = actorId;
            _logger = Context.GetLogger<SerilogLoggingAdapter>() 
                ?? throw new ArgumentNullException(nameof(SerilogLoggingAdapter));
        }

        protected override void PreStart()
            => _logger.Info($"{nameof(StatsCalculator)}-{ActorId} started");

        protected override void PostStop()
            => _logger.Info($"{nameof(StatsCalculator)}-{ActorId} stopped");

        protected override void OnReceive(object message)
        {
            switch (message)
            {
                case StatsCalculationCommand com when com.ModelId.Equals(_akkaModel.Id):
                    Calculate();
                    Sender.Tell(new StatsCalculationFinishedCommand(Guid.NewGuid(), ActorId, _akkaModel.Group.Id));
                    break;
                case StatsCalculationCommand com:
                    _logger.Warning($"Ignoring calculation request for {com.ModelId}. This actor is responsible for {_akkaModel.Id}.");
                    break;
                default:
                    _logger.Warning("Message of unrecognized type has been received");
                    break;
            }
        }

        public static Props Props(Guid actorId, AkkaModel akkaModel)
            => Akka.Actor.Props.Create(()
                => new StatsCalculator(actorId, akkaModel));

        private void Calculate()
        {
            /*
            Random random = new Random();
            var rnd = random.Next(1, 4);
            if (rnd % 3 == 0)
            {
                throw new ArgumentException("Something goes wrong...");
            }
            */

            try
            {
                _logger.Info($"Starting calculate stats for model {ActorId}");
                if (_akkaModel.AkkaSubModels != null)
                {
                    _logger.Info($"Starting calculate sub stats for model {ActorId}");
                    Parallel.ForEach(_akkaModel.AkkaSubModels, subModel =>
                    {
                        subModel.CalculateSubStats();
                    });
                    _logger.Info($"Sub stats calculation for model {ActorId} finished");
                }
                _akkaModel.CalculateStats();
                _logger.Info($"Stats calculation for model {ActorId} finished");
            }
            catch (Exception ex)
            {
                _logger.Error($"An unhandled exception has occurred: {ex.Message}");
            }
        }
    }
}
