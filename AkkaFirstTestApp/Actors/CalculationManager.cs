﻿using Akka.Actor;
using Akka.Event;
using Akka.Logger.Serilog;
using AkkaFirstTestApp.Commands;
using AkkaFirstTestApp.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace AkkaFirstTestApp.Actors
{
    public class CalculationManager : UntypedActor
    {
        private readonly IDbContextFactory<AkkaDbContext> _dbContextFactory;
        private readonly ILoggingAdapter _logger;
        private Dictionary<Guid, IActorRef> groupIdToActor = new Dictionary<Guid, IActorRef>();
        private Dictionary<IActorRef, Guid> actorToGroupId = new Dictionary<IActorRef, Guid>();
        private Guid ActorId { get; }


        public CalculationManager(
            IDbContextFactory<AkkaDbContext> dbContextFactory,
            Guid actorId)
        {
            _dbContextFactory = dbContextFactory;
            _logger = Context.GetLogger<SerilogLoggingAdapter>();
            ActorId = actorId;
        }

        protected override void PreStart()
            => _logger.Info($"{nameof(CalculationManager)}-{ActorId} started");

        protected override void PostStop()
            => _logger.Info($"{nameof(CalculationManager)}-{ActorId} stopped");

        protected override SupervisorStrategy SupervisorStrategy()
        {
            var strategy = new OneForOneStrategy(
                maxNrOfRetries: 10,
                withinTimeRange: TimeSpan.FromMilliseconds(200),
                localOnlyDecider: ex =>
                {
                    switch (ex)
                    {
                        case TimeoutException te:
                            return Directive.Stop;
                        case ArgumentException are:
                            _logger.Error($"An argument exception was occured: {are.Message}");
                            return Directive.Restart;
                        default:
                            return Directive.Escalate;
                    }
                });

            return strategy;
        }

        protected override void OnReceive(object message)
        {
            switch (message)
            {
                case InitiateFullCalculationCommand com:
                    _logger.Info($"Starting full calculation with command id: {com.CommandId}...");
                    OrchestrateGroups();
                    break;
                case InitiateGroupCalculationCommand com:
                    _logger.Info($"Starting calculation for group {com.GroupId} with command: {com.CommandId}...");
                    OrchestrateSingleGroup(com.GroupId);
                    break;
                case GroupCalculationFinishedCommand com:
                    if (groupIdToActor.TryGetValue(com.GroupId, out var actorRef))
                    {
                        Context.Stop(actorRef);
                        groupIdToActor.Remove(com.GroupId);
                        if (actorToGroupId.Count == 0)
                        {
                            _logger.Info("Calculation finished");
                        }
                    }
                    break;
                case Terminated t:
                    if (actorToGroupId.TryGetValue(t.ActorRef, out var actorId))
                    {
                        _logger.Info($"Actor {actorId} has been terminated");
                        actorToGroupId.Remove(t.ActorRef);
                        groupIdToActor.Remove(actorId);
                        if (actorToGroupId.Count == 0)
                        {
                            _logger.Info("Calculation finished");
                        }
                    }
                    break;
                default:
                    _logger.Warning("Message of unrecognized type has been received");
                    break;
            }
        }

        private void OrchestrateGroups()
        {
            _logger.Info("Creating actors for groups...");
            var dbContext = _dbContextFactory.CreateDbContext();

            var groups = dbContext.Groups.ToList();

            foreach (var group in groups)
            {
                var groupActor = Context.ActorOf(GroupManager.Props(group.Id, group, _dbContextFactory.CreateDbContext()));
                Context.Watch(groupActor);
                groupIdToActor.Add(group.Id, groupActor);
                actorToGroupId.Add(groupActor, group.Id);

                groupActor.Tell(new GroupCalculationCommad(Guid.NewGuid(), group.Id));
            }

            dbContext.Dispose();
            _logger.Info("Actors for groups created");
        }

        private void OrchestrateSingleGroup(Guid groupId)
        {
            _logger.Info($"Creating actor for group {groupId}...");
            var dbContext = _dbContextFactory.CreateDbContext();
            var group = dbContext.Groups.SingleOrDefault(g => g.Id.Equals(groupId));

            if (group != null)
            {
                var groupActor = Context.ActorOf(GroupManager.Props(group.Id, group, _dbContextFactory.CreateDbContext()));
                Context.Watch(groupActor);
                groupIdToActor.Add(group.Id, groupActor);
                actorToGroupId.Add(groupActor, group.Id);

                groupActor.Tell(new GroupCalculationCommad(Guid.NewGuid(), group.Id));
            }
            else
            {
                _logger.Warning($"Group with id {groupId} cannot be found");
            }
        }

        public static Props Props(IDbContextFactory<AkkaDbContext> dbContextFactory, Guid actorId)
            => Akka.Actor.Props.Create(()
                => new CalculationManager(dbContextFactory, actorId));
    }
}
