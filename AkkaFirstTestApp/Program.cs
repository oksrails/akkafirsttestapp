﻿using Akka.Actor;
using AkkaFirstTestApp;
using AkkaFirstTestApp.Actors;
using AkkaFirstTestApp.Commands;
using AkkaFirstTestApp.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

var startup = new Startup();

Console.WriteLine("Hello, Akka!");
Console.WriteLine("Enter number of test models to generate");

if (int.TryParse(Console.ReadLine(), out int modelCount))
{
    try
    {
        Console.WriteLine("Migrating data...");

        var dbContextFactory = startup.Provider.GetRequiredService<IDbContextFactory<AkkaDbContext>>();
        var dbContext = dbContextFactory.CreateDbContext();
        await dbContext.Migrate(modelCount);
        dbContext.Dispose();

        Console.WriteLine($"Migration successfully complete. Added {modelCount} models.{Environment.NewLine}Enter \"all\" to calculate all stats or guid of the specific group.");

        var answer = Console.ReadLine();

        var akkaSystem = startup.Provider.GetRequiredService<ActorSystem>();
        var calculationManager = akkaSystem.ActorOf(CalculationManager.Props(dbContextFactory, Guid.NewGuid()));

        if (answer == "all")
        {
            
            calculationManager.Tell(new InitiateFullCalculationCommand(Guid.NewGuid()));
        }

        if (Guid.TryParse(answer, out Guid result))
        {
            calculationManager.Tell(new InitiateGroupCalculationCommand(Guid.NewGuid(), result));
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine($"An unhandled exception has occurred: {ex.ToString()}");
    }
}

Console.ReadLine();