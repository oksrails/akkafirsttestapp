﻿using AkkaFirstTestApp.Domains;

namespace AkkaFirstTestApp.Infrastructure
{
    public sealed class DataGenerator
    {
        public List<Group> Groups { get; private set; }
        public List<AkkaSubModel> AkkaSubModels { get; private set; }
        public List<AkkaModel> AkkaModels { get; private set; }

        private List<string> _groupNames = new List<string> { "Group1", "Group2", "Group3" };

        private List<string> _modelNameParts = new List<string> 
        {
            "aa",
            "cj",
            "cl",
            "cr",
            "pj",
            "lg",
            "rf",
            "yb",
            "uv",
            "ws",
            "qz",
            "rd",
            "ig",
            "hf",
            "zx",
            "tb",
            "ve",
        };

        private List<AkkaSubModel> _subModels;
        private List<AkkaModel> _akkaModels;

        public DataGenerator(int modelCount)
        {
            Groups = new List<Group>();
            AkkaModels = new List<AkkaModel>();

            foreach (string groupName in _groupNames)
            {
                Groups.Add(
                    new Group 
                    {
                        Id = Guid.NewGuid(),
                        Name = groupName
                    });
            }

            for (int i = 0; i <= modelCount; i++)
            {
                var akkaModel = new AkkaModel();
                akkaModel.Id = Guid.NewGuid();
                akkaModel.Name = GenerateName();
                akkaModel.Group = GetGroup();
                akkaModel.StatA = GetStat();
                akkaModel.AkkaSubModels = new List<AkkaSubModel>();

                for (int j = 0; j <= 10; j++)
                {
                    var akkaSubModel = new AkkaSubModel
                    {
                        Id = Guid.NewGuid(),
                        Name = GenerateName(),
                        SubStatA = GetStat()
                    };
                    akkaModel.AkkaSubModels.Add(akkaSubModel);
                }

                AkkaModels.Add(akkaModel);
            }
        }

        private Group GetGroup()
        {
            Random random = new Random();
            return Groups[random.Next(3)];
        }

        private double GetStat()
        {
            Random random = new Random();
            return (double)random.NextDouble();
        }

        private string GenerateName()
        {
            Random random = new Random();
            var maxNumber = _modelNameParts.Count;
            return _modelNameParts[random.Next(maxNumber)] + _modelNameParts[random.Next(maxNumber)] + _modelNameParts[random.Next(maxNumber)];
        }
    }
}
