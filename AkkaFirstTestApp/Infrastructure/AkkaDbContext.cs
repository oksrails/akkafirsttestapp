﻿using AkkaFirstTestApp.Domains;
using Microsoft.EntityFrameworkCore;

namespace AkkaFirstTestApp.Infrastructure
{
    public class AkkaDbContext : DbContext
    {
        public DbSet<Group> Groups { get; set; }
        public DbSet<AkkaSubModel> AkkaSubModels { get; set; }
        public DbSet<AkkaModel> AkkaModels { get; set; }

        public AkkaDbContext(DbContextOptions<AkkaDbContext> options)
            : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder dbContextOptionsBuilder)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public async Task Migrate(int modelCount = 0)
        {
            this.Database.Migrate();
            if (modelCount > 0)
            {
                DataGenerator dataGenerator = new DataGenerator(modelCount);
                await Groups.AddRangeAsync(dataGenerator.Groups);
                await AkkaModels.AddRangeAsync(dataGenerator.AkkaModels);
                await this.SaveChangesAsync();
            }
        }
    }
}
