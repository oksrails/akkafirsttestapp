﻿using Akka.Actor;
using Akka.Configuration;
using AkkaFirstTestApp.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace AkkaFirstTestApp
{
    public class Startup
    {
        private readonly IConfiguration configuration;
        private readonly IServiceProvider provider;

        public IServiceProvider Provider => provider;
        public IConfiguration Configuration => configuration;

        public Startup()
        {
            configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            var services = new ServiceCollection();
            services.AddSingleton<IConfiguration>(configuration);

            Log.Logger = GetLogger();

            services.AddDbContextFactory<AkkaDbContext>(options => 
                options.UseNpgsql(Configuration["ConnectionStrings:PostgreConnectionString"]));

            var actorSystem = ConfigureAkka(Configuration["Akka"]);

            services.AddSingleton<ActorSystem>(actorSystem);

            provider = services.BuildServiceProvider();
        }

        static ActorSystem ConfigureAkka(string configuration)
        {
            var systemConfiguration = new Config();
            systemConfiguration += configuration;

            return ActorSystem.Create("first-test-system", systemConfiguration);
        }

        static ILogger GetLogger()
        {
            var logger = new LoggerConfiguration()
                .WriteTo.Console()
                .WriteTo.File(Path.Combine(Environment.CurrentDirectory, "akka_logs.txt"))
                .MinimumLevel.Verbose()
                .CreateLogger();

            return logger;
        }
    }
}
