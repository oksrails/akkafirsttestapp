﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AkkaFirstTestApp.Migrations
{
    public partial class InitialMigrate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AkkaModels",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    GroupId = table.Column<Guid>(type: "uuid", nullable: false),
                    StatA = table.Column<double>(type: "double precision", nullable: false),
                    StatB = table.Column<double>(type: "double precision", nullable: false),
                    StatC = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AkkaModels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AkkaModels_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AkkaSubModels",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    SubStatA = table.Column<double>(type: "double precision", nullable: false),
                    SubStatB = table.Column<double>(type: "double precision", nullable: false),
                    SubStatC = table.Column<double>(type: "double precision", nullable: false),
                    AkkaModelId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AkkaSubModels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AkkaSubModels_AkkaModels_AkkaModelId",
                        column: x => x.AkkaModelId,
                        principalTable: "AkkaModels",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_AkkaModels_GroupId",
                table: "AkkaModels",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_AkkaSubModels_AkkaModelId",
                table: "AkkaSubModels",
                column: "AkkaModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AkkaSubModels");

            migrationBuilder.DropTable(
                name: "AkkaModels");

            migrationBuilder.DropTable(
                name: "Groups");
        }
    }
}
